# spirit-ai-test
This repository contains the software developed by Matteo Vettosi for the Technical Test requested by Spirit AI as part
of the interview process.
This small document contains a small discussion of the technologies used, then proceeds with a quick analysis of each of
of the tasks completed, illustrates how to run the whole project and concludes with some considerations on the work done.

## Technologies used
The programming language chosen to develop the first three tasks is Python: while not being the strongest technology
known to the author, the inherent and primary scripting nature of the requested application, along with small size and
complexity, ultimately designed this language as the best suited for the purpose.

In particular, for the first two tasks no external libraries were being used while for the third the Flask web server
was used over Django to serve the web api layer. This decision was motivated again after the extreme simplicity of the
purpose that, not including any model, database or particular deployment restriction or requirement, would not have
justified the overhead introduced by the usage of a much more developed and complete framework like Django.
The framework used to write and launch the unit tests was the default python's unittest module, being quite simple and
powerful while not requiring any additional installation.

For the task 4, the frontend objective was chosen and it was decided to be developed as a SPA (Single Page Application)
in Vue.js, perfectly suiting the web service setup of the previous task and leveraging the ability of the web
application to run everywhere a browser works without any repackaging or installation needed while achieving
desktop-like performances since the resulted application is compiled into a static html file, completely fetched by the
client during the first connection to the server and ran as a local program inside the browser.

## Task 1: FizzBuzz
Not much to say about this task, the algorithm implementation was pretty straight forward with a complexity of O(1).
The 20 rows used to write the check function were judged the bare minimum to ensure adequate readability.
The command-line-like functionality of printing the first 100 results of the check function was judged to be too simple
for a unit test, so the developed suite focused on ensuring the correct functioning of the check_number and is_fib
functions.

## Task 2: Roman calculator
This task, as the previous one, was implemented without using any library: while more than one were available,
implementing pretty much everything requested, it was considered pointless to use them and limit the implementation to
be just a couple of library calls.
Both the conversion functions operate in O(n) time, with n being the length of the string representation of the input,
and further optimizations, while possible, were avoided in favour of readability since the algorithmic complexity could
not be improved further.
Even thought both of them correctly validate their input, for the roman_to_arabic function there ware some hard times
trying to find an extensive and official list of syntactical rules to ensure that a roman number is correctly formed:
since many were found conflicting with each other, the more complete one found
[here](https://www.factmonster.com/math/numbers/roman-numerals); moreover, since different opinions are available
regarding the representation of the numbers above 3999, the most common one adopted online was chosen, being simply
using the related number of 'M's, breaking the rule that no more than three consecutive should be used. This was mainly
because the additional characters, beings the same letters with either a line or other symbols above them, to represent
the thousands are extremely difficult to type in modern keyboards.

Finally, the calculator was realized by exploiting the capability of the Python language to evaluate mathematical
expressions natively, solution that clearly differs from the decision to avoid libraries for the roman conversion since
the functionality is provided by the language itself. To maximize security, however, the expression passed to the eval
function is checked against all the allowed characters expected and the eval environment itself is stripped out by all
the other python functions and keywords.

## Task 3: WebApi
Thanks to the minimality of both the webserver flask and the requirements themselves, the python file implementing this
tasks results quite contained and simple: noteworthy are just the registration of an error handler to avoid HTTP 500s in
case of validation error or similar, and the usage of a custom path converter to ensure that an expression could be
used in an api path. All the apis were implemented following the REST standards as much as possible considering that no
entities or CRUD operations were involved.

## Task 4: WebUI
The file structure of the project was realized using the vue-cli utility and, while being ready for both unit and e2e
testing, the actual implementation of said tests for a UI component at this point was considered out of the scope.
During development the ui project was hosted by nodejs to enable hot-swapping of the code while using the apis hosted by
the flask instance using both a CORS library on the python side (now commented) and an alternative axios http client on
the client side.
For production, the frontend is meant to be compiled and served as static resource by the Flask server, as shown in the
following section.

## How to run
The application was tested with both Python 3.5 and 3.7 and while nodejs and npm are needed to compile the frontend
static files, an pre-built version is provided in the tasks/frontend/dist folder for convenience.

### Compile the webui (optional)
```
cd tasks/frontend
npm run build
cd ../../
```

### Install the python project (using pipenv)
```
pipenv install
pipenv shell
```

### Install the python project (using virtualenv)
```
virtualenv -p python3 venv
source venv/bin/activate
pip install -r requirements.txt
```

### Run the server
```
cd tasks/
FLASK_APP=webapi.py flask run
```

### Run the unit tests
```
cd ../
python -m unittest
```

# Comment from the author
I hope you will have as much fun in evaluating this project as I did in developing it, and while obviously there is
always space for improvement I hope I decided to go for the right level of complexity and completeness of project of
this kind can have.
The application is currently hosted on Heroku at [https://mvettosi-spirit-ai.herokuapp.com](https://mvettosi-spirit-ai.herokuapp.com/)