#! /bin/bash

if [ "$1" = "-h" ]; then
    echo "Usage: $0 [-dev]"
    exit 0
fi

cd $(realpath $(dirname $0))/tasks/
export FLASK_APP=webapi.py
if [ "$1" = "-dev" ]; then
    export FLASK_ENV=development
fi
flask run
