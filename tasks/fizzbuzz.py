# Write a program that prints the numbers from 0 to 100. But for multiples of three print
# “Fizz” instead of the number and for multiples of five prints “Buzz”. For numbers which are
# multiples of both three and five print “FizzBuzz”. Then extend the program to say
# “Flamingo” when a number is a member of the Fibonacci sequence, and “Pink Flamingo”
# when it is a multiple of 3 and 5 and a member of the Fibonacci sequence. Hint: A number is
# Fibonacci if and only if one or both of (5*n2 + 4) or (5*n2 – 4) is a perfect square (i.e. A
# number made by squaring a whole number).
from math import sqrt


def is_fib(n):
    """Returns True if n belongs to the Fibonacci Sequence, false otherwise"""
    return sqrt(5 * n ** 2 + 4).is_integer() or sqrt(5 * n ** 2 - 4).is_integer()


def check_number(number):
    """Analyzes the integer argument i and returns either itself or a string representation
     following the programs directions
     """
    if not isinstance(number, int):
        raise ValueError('Input "{}" is not an integer'.format(number))
    result = ''
    if number % 3 == 0:
        result += 'Fizz'
    if number % 5 == 0:
        result += 'Buzz'
    if is_fib(number):
        if result == 'FizzBuzz':
            result = 'Pink '
        else:
            result = ''
        result += 'Flamingo'
    if result == '':
        result = number
    return result


def print_numbers(n=100):
    """Prints the result of the check_number function on the first n integers"""
    for i in range(n):
        print(check_number(i))


if __name__ == '__main__':
    print_numbers()
