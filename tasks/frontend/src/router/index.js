import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Home'
import FizzBuzz from '@/components/FizzBuzz'
import Converter from '@/components/Converter'
import Calculator from '@/components/Calculator'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      path: '/fizzbuzz',
      name: 'fizzbuzz',
      component: FizzBuzz
    },
    {
      path: '/converter',
      name: 'converter',
      component: Converter
    },
    {
      path: '/calculator',
      name: 'calculator',
      component: Calculator
    }
  ]
})
