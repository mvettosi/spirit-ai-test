# Start by writing a Roman Numeral converter and from that Roman Numerals. Then write a
# Roman Numeral Calculator that takes a string i.e. “XXIV + XI” and returns the result which
# would be “XXXV”. Ideally it should perform all BODMAS calculation so:
# Brackets: ()
# Order (power): ^
# Division: /
# Multiplication: *
# Addition: +
# Subtraction: -
import re
from enum import IntEnum


class Symbol(IntEnum):
    """Enumeration encapsulating roman symbols and values, and operations available on them"""
    I = 1
    V = 5
    X = 10
    L = 50
    C = 100
    D = 500
    M = 1000

    @classmethod
    def get_by_power(cls, power, digit=1):
        return Symbol(digit * 10 ** power).name

    @classmethod
    def get_closest_pivot(cls, n):
        if n in [0, 1, 2, 3]:
            return 0
        elif n in [4, 5, 6, 7, 8]:
            return 5
        else:
            return 10


roman_symbols = [s.name for s in Symbol]
expression_regex = '[{}+\-*/^() ]+?'.format("".join(roman_symbols))


class MalformedRomanNumberError(Exception):
    """General exception for syntactic violations"""
    pass


def roman_to_arabic(number):
    """Receives a roman number as a string and returns the corresponding arabic representation"""
    if not isinstance(number, str):
        raise ValueError('The argument passed was {} instead of a string'.format(type(number)))
    if any(ch not in roman_symbols for ch in number):
        raise ValueError('The argument "{}" passed is not a roman number'.format(number))
    number = number.upper()
    last_symbol = None
    last_symbol_count = 0
    result = 0
    for index, symbol_name in enumerate(number):
        # Esaminate each symbol taking into account the following one, if any
        symbol = Symbol[symbol_name]
        next_symbol = Symbol[number[index + 1]] if index < len(number) - 1 else None
        if next_symbol is None or next_symbol <= symbol:
            if symbol == last_symbol and last_symbol_count >= 3 and symbol != Symbol.M:
                raise MalformedRomanNumberError('Cannot repeat same symbol more than three times')
            result += symbol
        else:
            # next_symbol < symbol
            if symbol not in [Symbol.I, Symbol.X, Symbol.C, Symbol.M]:
                raise MalformedRomanNumberError('Cannot subtract a number that is not a power of ten')
            if symbol == last_symbol:
                raise MalformedRomanNumberError('Cannot subtract more than one number at the same time')
            if symbol * 10 < next_symbol:
                raise MalformedRomanNumberError('Cannot subtract a number that is more than ten times smaller')
            result -= symbol
        if symbol != last_symbol:
            # New symbol, reset repetition count to one
            last_symbol_count = 1
        else:
            # Not a new one, increase that instead
            last_symbol_count += 1
        last_symbol = symbol
    return result


def arabic_to_roman(number):
    """Receives an arabic number as an integer and returns the corresponding roman representation"""
    if isinstance(number, int):
        # We need a string representation of the number, in order to iterate on the digits
        number = str(number)
    elif not isinstance(number, str):
        raise ValueError('The argument "{}" passed is not an integer or a string'.format(number))
    elif not number.isdigit():
        raise ValueError('The argument "{}" provided is not an integer'.format(number))
    result = ''
    for index, digit_string in enumerate(reversed(number[-3:])):
        # Convert first three digits
        digit = int(digit_string)
        if digit == 0:
            # No representation needed for 0
            continue
        # Get central number of the roman digit, that might need addition or subtraction
        pivot = Symbol.get_closest_pivot(digit)
        roman_digit = Symbol.get_by_power(index, pivot) if pivot > 0 else ''
        # Calculate how much needs to be added to the pivot
        to_be_added = digit % 5
        if digit in [4, 9]:
            # Needs a subtraction
            roman_digit = Symbol.get_by_power(index) + roman_digit
        elif to_be_added > 0:
            # Needs some addition
            roman_digit += Symbol.get_by_power(index) * to_be_added
        result = roman_digit + result
    if len(number) > 3:
        # Add thousands
        result = Symbol.M.name * int(number[:-3]) + result
    return result


def roman_calculator(expression):
    if not isinstance(expression, str):
        raise ValueError('Only strings can be parsed as expressions')
    if not re.match(expression_regex, expression):
        raise ValueError('Expression "{}" provided is not valid. Please only use any of the following characters: '
                         '{}+-*/^() '.format(expression, roman_symbols))
    # Convert the provided '^' symbol with the python representation '**'
    expression = re.sub(r'\^', '**', expression)
    # Convert the provided '/' symbol with the python floor division '//'
    expression = re.sub(r'/', '//', expression)
    # Convert the provided roman numbers into arabics
    expression = re.sub(r'[^+\-*/^() ]+', lambda match: str(roman_to_arabic(str(match.group()))), expression)
    # Evaluate expression safely, disabling anything other than builtin symbols
    result = eval(expression, {'__builtins__': {}})
    # Ensure the result is
    return arabic_to_roman(result)
