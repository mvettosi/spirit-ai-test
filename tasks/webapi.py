from flask import Flask, jsonify, render_template
from werkzeug.routing import PathConverter

from tasks import fizzbuzz
from tasks import roman_numeral_calculator
# from flask_cors import CORS

app = Flask(__name__, static_folder="./frontend/dist/static", template_folder="./frontend/dist")
# Used during development to call flask apis from the frontend hosted by nodejs
# cors = CORS(app, resources={r"/api/*": {"origins": "*"}})


# Registering a custom converter that can accept paths with encoded slashes
class ExpressionConverter(PathConverter):
    regex = roman_numeral_calculator.expression_regex


app.url_map.converters['expr'] = ExpressionConverter


# Error handlers
@app.errorhandler(Exception)
def special_exception_handler(error):
    message = [str(x) for x in error.args]
    response = {
        'type': error.__class__.__name__,
        'message': message
    }
    return jsonify(response), 400


# Fizzbuzz
@app.route('/api/fizzbuzz/<int:number>')
def check_fizzbuzz_number(number):
    return jsonify({'number': fizzbuzz.check_number(number)})


@app.route('/api/isfib/<int:number>')
def check_is_fib(number):
    return jsonify({'isfib': fizzbuzz.is_fib(number)})


# Romans
@app.route('/api/roman/<number>')
def roman(number):
    if number.isdigit():
        roman_number = roman_numeral_calculator.arabic_to_roman(number)
        arabic_number = number
    else:
        # The argument is not a digit, so roman_to_arabic will take care of further validations
        roman_number = number
        arabic_number = roman_numeral_calculator.roman_to_arabic(number)
    return jsonify({'roman': roman_number, 'arabic': int(arabic_number)})


@app.route('/api/roman/calculate/<expr:expression>')
def calculate_roman(expression):
    result = roman_numeral_calculator.roman_calculator(expression)
    return jsonify({'result': result})


# WebUI
@app.route('/')
def webui():
    return render_template('index.html')


if __name__ == '__main__':
    app.run()
