# Write a program that prints the numbers from 0 to 100. But for multiples of three print
# “Fizz” instead of the number and for multiples of five prints “Buzz”. For numbers which are
# multiples of both three and five print “FizzBuzz”. Then extend the program to say
# “Flamingo” when a number is a member of the Fibonacci sequence, and “Pink Flamingo”
# when it is a multiple of 3 and 5 and a member of the Fibonacci sequence. Hint: A number is
# Fibonacci if and only if one or both of (5*n2 + 4) or (5*n2 – 4) is a perfect square (i.e. A
# number made by squaring a whole number).

import unittest

from tasks.fizzbuzz import *


class FizzBuzzTests(unittest.TestCase):
    repeatList = [4, 7, 11, 14, 16, 17, 19, 22, 23, 26, 28, 29, 31, 32, 37, 38, 41, 43, 44, 46, 47, 49, 52, 53, 56, 58,
                  59, 61, 62, 64, 67, 68, 71, 73, 74, 76, 77, 79, 82, 83, 86, 88, 91, 92, 94, 97, 98]
    fizzList = [6, 9, 12, 18, 24, 27, 33, 36, 39, 42, 48, 51, 54, 57, 63, 66, 69, 72, 78, 81, 84, 87, 93, 96, 99]
    buzzList = [10, 20, 25, 35, 40, 50, 65, 70, 80, 85, 95, 100]
    fizzBuzzList = [15, 30, 45, 60, 75, 90]
    flamingoList = [1, 2, 3, 5, 8, 13, 21, 34, 55, 89]

    def test_check_number_repeats_input(self):
        for x in self.repeatList:
            self.assertEqual(x, check_number(x))

    def test_check_number_fizz(self):
        for x in self.fizzList:
            self.assertEqual('Fizz', check_number(x))

    def test_check_number_buzz(self):
        for x in self.buzzList:
            self.assertEqual('Buzz', check_number(x))

    def test_check_number_fizzbuzz(self):
        for x in self.fizzBuzzList:
            self.assertEqual('FizzBuzz', check_number(x))

    def test_check_number_flamingo(self):
        for x in self.flamingoList:
            self.assertEqual('Flamingo', check_number(x))

    def test_check_number_pinkflamingo(self):
        self.assertEqual('Pink Flamingo', check_number(6765))

    def test_check_number_rejects_non_int(self):
        with self.assertRaises(ValueError):
            check_number('hello')

    def test_fib_true(self):
        self.assertEqual(True, is_fib(6765))

    def test_fib_false(self):
        self.assertEqual(False, is_fib(77))


if __name__ == '__main__':
    unittest.main()
