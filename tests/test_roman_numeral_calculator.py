# Start by writing a Roman Numeral converter and from that Roman Numerals. Then write a
# Roman Numeral Calculator that takes a string i.e. “XXIV + XI” and returns the result which
# would be “XXXV”. Ideally it should perform all BODMAS calculation so:
# Brackets: ()
# Order (power): ^
# Division: /
# Multiplication: *
# Addition: +
# Subtraction: -

import unittest

from tasks.roman_numeral_calculator import *


class RomanToArabicTests(unittest.TestCase):
    def test_roman_to_arabic_not_string_issued(self):
        """Checks that passing a non-string as argument throws an exception"""
        with self.assertRaises(ValueError):
            roman_to_arabic(5)

    def test_roman_to_arabic_repeated_symbol(self):
        """Checks that a number comprised by only a single repeated symbol is parsed correctly"""
        self.assertEqual(30, roman_to_arabic('XXX'))

    def test_roman_to_arabic_more_than_three_repetition(self):
        """Checks that repeating the same symbol more than three times throws an exception"""
        with self.assertRaises(MalformedRomanNumberError):
            roman_to_arabic('XXXX')

    def test_roman_to_arabic_addition_of_decreasing_symbols(self):
        """Checks the addition mechanism for a decreasing-only sequence of symbols"""
        self.assertEqual(6, roman_to_arabic('VI'))
        self.assertEqual(70, roman_to_arabic('LXX'))
        self.assertEqual(1200, roman_to_arabic('MCC'))

    def test_roman_to_arabic_subtraction_of_increasing_symbols(self):
        """Checks the subtraction mechanism for an increasing sequence of symbols"""
        self.assertEqual(4, roman_to_arabic('IV'))
        self.assertEqual(90, roman_to_arabic('XC'))
        self.assertEqual(900, roman_to_arabic('CM'))

    def test_roman_to_arabic_mixed_additions_and_subtractions(self):
        """Checks the addition and subtraction mechanisms works well while mixed"""
        self.assertEqual(1958, roman_to_arabic('MCMLVIII'))
        self.assertEqual(2049, roman_to_arabic('MMXLIX'))
        self.assertEqual(84, roman_to_arabic('LXXXIV'))

    def test_roman_to_arabic_only_subtract_power_of_ten(self):
        """Checks that subtracting something different from a power of ten throws an exception"""
        with self.assertRaises(MalformedRomanNumberError):
            roman_to_arabic('VC')

    def test_roman_to_arabic_only_subtract_one_per_time(self):
        """Checks that subtracting more than one value at the same time throws an exception"""
        with self.assertRaises(MalformedRomanNumberError):
            roman_to_arabic('IIXV')

    def test_roman_to_arabic_only_subtract_less_than_ten_times_greater(self):
        """Checks that subtracting a value more than ten times smaller than the following one times throws an exception"""
        with self.assertRaises(MalformedRomanNumberError):
            roman_to_arabic('IC')

    def test_oman_to_arabic__check_greater_than_or_equal_to_four_thousands(self):
        """Checks that arabic_to_roman correctly generates a number that is greater than 3999"""
        self.assertEqual(7543, roman_to_arabic('MMMMMMMDXLIII'))

    def test_roman_to_arabic_forbidden_chars(self):
        """Checks that passing a string with extraneous characters as argument throws an exception"""
        with self.assertRaises(ValueError):
            roman_to_arabic('XVaI')


class ArabicToRomanTests(unittest.TestCase):
    def test_roman_to_arabic_not_string_issued(self):
        """Checks that passing a non-integer and non string as argument throws an exception"""
        with self.assertRaises(ValueError):
            arabic_to_roman('5a')

    def test_arabic_to_roman_check_single_symbol_numbers(self):
        """Checks that conversion of the base symbols works"""
        self.assertEqual('I', arabic_to_roman(1))
        self.assertEqual('V', arabic_to_roman(5))
        self.assertEqual('X', arabic_to_roman(10))
        self.assertEqual('L', arabic_to_roman(50))
        self.assertEqual('C', arabic_to_roman(100))
        self.assertEqual('D', arabic_to_roman(500))
        self.assertEqual('M', arabic_to_roman(1000))

    def test_arabic_to_roman_check_repeated_digits(self):
        """Checks that arabic_to_roman correctly generates a number with repeated digits"""
        self.assertEqual('XXX', arabic_to_roman(30))

    def test_arabic_to_roman_check_additions(self):
        """Checks that arabic_to_roman correctly generates a number that requires additions"""
        self.assertEqual('VI', arabic_to_roman(6))
        self.assertEqual('LXX', arabic_to_roman(70))
        self.assertEqual('MCC', arabic_to_roman(1200))

    def test_arabic_to_roman_check_subtractions(self):
        """Checks that arabic_to_roman correctly generates a number that requires subtractions"""
        self.assertEqual('IV', arabic_to_roman(4))
        self.assertEqual('XC', arabic_to_roman(90))
        self.assertEqual('CM', arabic_to_roman(900))

    def test_arabic_to_roman_check_mixed(self):
        """Checks that arabic_to_roman correctly generates a number that requires mixed operations"""
        self.assertEqual('MCMLVIII', arabic_to_roman(1958))
        self.assertEqual('MMXLIX', arabic_to_roman(2049))
        self.assertEqual('LXXXIV', arabic_to_roman(84))

    def test_roman_to_arabic_error_on_negative_number(self):
        """Checks that passing a negative integer throws an exception"""
        with self.assertRaises(ValueError):
            arabic_to_roman('-5')

    def test_arabic_to_roman_check_greater_than_or_equal_to_four_thousands(self):
        """Checks that arabic_to_roman correctly generates a number that is greater than 3999"""
        self.assertEqual('MMMMMMMDXLIII', arabic_to_roman(7543))


class RomanCalculatorTests(unittest.TestCase):
    def test_roman_calculator_rejects_non_strings(self):
        """Checks that passing something different from a string throws an exception"""
        with self.assertRaises(ValueError):
            roman_calculator(5)

    def test_roman_calculator_parse_additions(self):
        """Checks that the roman calculator can correctly parse an addition"""
        self.assertEqual('XVI', roman_calculator('XI + V'))

    def test_roman_calculator_parse_subtractions(self):
        """Checks that the roman calculator can correctly parse a subtraction"""
        self.assertEqual('VI', roman_calculator('XI - V'))

    def test_roman_calculator_parse_multiplications(self):
        """Checks that the roman calculator can correctly parse a multiplication"""
        self.assertEqual('LV', roman_calculator('XI * V'))

    def test_roman_calculator_parse_division(self):
        """Checks that the roman calculator can correctly parse a division"""
        self.assertEqual('XI', roman_calculator('LV / V'))

    def test_roman_calculator_parse_power(self):
        """Checks that the roman calculator can correctly parse a power"""
        self.assertEqual('XXXII', roman_calculator('II ^ V'))

    def test_roman_calculator_parse_brackets(self):
        """Checks that the roman calculator can correctly parse an expression with parenthesis"""
        self.assertEqual('XXXVI', roman_calculator('II * (XVI + II)'))

    def test_roman_calculator_parse_complex_expr(self):
        """Checks that the roman calculator can correctly parse an expression with all the symbols"""
        self.assertEqual('XXIV', roman_calculator('(I + I) ^ IV * III / II'))


if __name__ == '__main__':
    unittest.main()
