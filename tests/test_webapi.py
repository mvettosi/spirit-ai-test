import unittest
import urllib.parse

from flask import json

from tasks import webapi


class FizzbuzzWebapiTests(unittest.TestCase):

    def setUp(self):
        webapi.app.testing = True
        self.app = webapi.app.test_client()

    def tearDown(self):
        pass

    def test_fizzbuzz_number_int(self):
        response = self.app.get('/api/fizzbuzz/4')
        self.assertEqual(200, response.status_code)
        data = json.loads(response.data)
        self.assertEqual(4, data['number'])

    def test_fizzbuzz_number_str(self):
        response = self.app.get('/api/fizzbuzz/3')
        self.assertEqual(200, response.status_code)
        data = json.loads(response.data)
        self.assertEqual('Flamingo', data['number'])

    def test_is_fib(self):
        response = self.app.get('/api/isfib/3')
        self.assertEqual(200, response.status_code)
        data = json.loads(response.data)
        self.assertEqual(True, data['isfib'])


class RomanCalculatorWebapiTests(unittest.TestCase):

    def setUp(self):
        webapi.app.testing = True
        self.app = webapi.app.test_client()

    def tearDown(self):
        pass

    def test_from_roman(self):
        response = self.app.get('/api/roman/MCMLVIII')
        self.assertEqual(200, response.status_code)
        data = json.loads(response.data)
        self.assertEqual('MCMLVIII', data['roman'])
        self.assertEqual(1958, data['arabic'])

    def test_from_arabic(self):
        response = self.app.get('/api/roman/1958')
        self.assertEqual(200, response.status_code)
        data = json.loads(response.data)
        self.assertEqual('MCMLVIII', data['roman'])
        self.assertEqual(1958, data['arabic'])

    def test_calculator(self):
        expression = '(I + I) ^ IV * III / II'
        urlencoded = urllib.parse.quote(expression)
        response = self.app.get('/api/roman/calculate/' + urlencoded)
        self.assertEqual(200, response.status_code)
        data = json.loads(response.data)
        self.assertEqual('XXIV', data['result'])


if __name__ == '__main__':
    unittest.main()